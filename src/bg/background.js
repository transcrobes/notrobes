'use strict';

function onError(error) {
  console.error(`Error: ${error}`);
}

browser.browserAction.onClicked.addListener(() => {
    const createData = {
        url: "ui/panel.html",
    };
    let creating = browser.tabs.create(createData);
    creating.then(() => {
        console.log("The normal tab has been created");
    }).catch(onError);
});
