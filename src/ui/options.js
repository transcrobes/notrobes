"use strict";

const usernameInput = document.querySelector("#username");
const passwordInput = document.querySelector("#password");
const baseUrlInput = document.querySelector("#base-url");

function storeSettings() {
  browser.storage.local.set({
    authCredentials: {
      username: usernameInput.value,
      password: passwordInput.value,
      baseUrl: baseUrlInput.value
    }
  });
}

function updateUI(restoredSettings) {
  usernameInput.value = restoredSettings.authCredentials.username || "";
  passwordInput.value = restoredSettings.authCredentials.password || "";
  baseUrlInput.value = restoredSettings.authCredentials.baseUrl || "";
}

function onError(e) {
  console.error(e);
}

const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(updateUI, onError);

/*
On blur, save the currently selected settings.
*/
usernameInput.addEventListener("blur", storeSettings);
passwordInput.addEventListener("blur", storeSettings);
baseUrlInput.addEventListener("blur", storeSettings);
