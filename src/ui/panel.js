"use strict";

let username = '';
let password = '';
let baseUrl = '';

function loadCredentialsAndRun(restoredSettings) {
    username = restoredSettings.authCredentials.username;
    password = restoredSettings.authCredentials.password;
    if (restoredSettings.authCredentials.baseUrl.endsWith('/')) {
        baseUrl = restoredSettings.authCredentials.baseUrl;
    } else {
        baseUrl = restoredSettings.authCredentials.baseUrl + '/';
    }
}

function onError(e) {
    console.error(e);
}

function runLoadCreds() {
    const gettingStoredSettings = browser.storage.local.get();
    gettingStoredSettings.then(loadCredentialsAndRun, onError);
}

let lastItemSearched = null;

function doCreateElement(elType, elClass, elInnerText, elAttrs, elParent) {
    if (!(elType)) { throw "eltype must be an element name"; };
    const el = document.createElement(elType);
    if (!!(elClass)) {
        el.classList.add(elClass);
    }
    if (!!(elInnerText) || elInnerText == '0') {
        el.textContent = elInnerText;
    }
    if (!!(elAttrs)) {
        for (let attr of elAttrs) {
            el.setAttribute(attr[0], attr[1]);
        }
    }
    if (!!(elParent)) {
        elParent.appendChild(el);
    }
    return el;
}
let i = 0;

function setNote(ev) {
    const even = document.getElementById("def" + ev.data.defId);

    const pinyin = even.dataset.pinyin;
    const chars = even.dataset.chars;
    const meaning = even.dataset.meaning;

    const tags = document.getElementById('noteTags').value.split(' ');

    const note = {"Simplified": chars, "Pinyin": pinyin, "Meaning": meaning, "Tags": tags};
    console.log("Sending json string in setNote: " + JSON.stringify(note));
    $.ajax({
        type: 'POST',
        cache: false,
        url: baseUrl + 'notes/set_word',
        data: JSON.stringify(note),
        datatype: 'json',
        contentType: 'application/json; charset=utf-8',
        headers: {
          "Authorization": "Basic " + btoa(username + ":" + password)
        },
        success: function(data) {
            console.log('Successfully created a note for: ' + chars);
            findNotes(null); //refresh the UI
            document.getElementById('actionResult').innerText = 'Successfully synced at : ' + new Date().toUTCString();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:'); console.log(jqXHR); console.log('textStatus:'); console.log(textStatus); console.log('errorThrown:'); console.log(errorThrown);
        }
    });
}

function iterProviders(key, value) {
    const main = doCreateElement('div', 'tc-provider', key, null, document.getElementById('output'));
    main.tabIndex = key;
    $.each(value, function(k, v) {
        const pyDef = doCreateElement('div', 'tc-py-def', null, null, main);
        const defHeader = doCreateElement('div', 'tc-defheader', null, null, pyDef);
        const butN = doCreateElement('img', 'tc-def-butN', null, [['src', "../img/plus.png"], ['id', 'def' + i]], defHeader)
        butN.dataset.pinyin = v['Pinyin'];
        butN.dataset.chars = document.getElementById('hz').value;
        butN.dataset.meaning = v['Meaning'];
        $(butN).click({ defId: i }, setNote);

        doCreateElement('span', 'tc-py', v['Pinyin'], null, defHeader);
        doCreateElement('span', 'tc-def', i + '. ', null, pyDef);
        doCreateElement('span', 'tc-defo', v['Meaning'], null, pyDef);
        if ('Tags' in v) {
            doCreateElement('div', 'tc-note-tags', 'Tags: ' + v['Tags'].join(', '), null, pyDef);
        }
        i++;
    });
    doCreateElement('hr', null, null, null, main);
}

function printInfos(info, parentDiv) {
    const infoDiv = doCreateElement('div', 'tc-stats', null, null, parentDiv);
    $.each(info, function(i) {
        var e = info[i];
        if (!!(e['metas'])) {
            const infoElem = doCreateElement('div', 'tc-' + e['name'] + 's', null, null, infoDiv);
            doCreateElement('div', 'tc-' + e['name'], e['metas'], null, infoElem);
        } else {
            doCreateElement('div', 'tc-' + e['name'], 'No ' + e['name'] + ' found', null, infoDiv);
        }
        doCreateElement('hr', null, null, null, infoDiv);
    })
}

var jqxhr = {abort: function () {}};
function findNotes(ev) {
    let searchItem = document.getElementById('hz').value.trim();
    if (!!(ev) && lastItemSearched == searchItem) {
        return; // not going to requery the same thing
    }

    var callingState = new Error(); // capture state/stack when ajax is called
    jqxhr.abort();

    jqxhr = $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: baseUrl + 'enrich/word_definitions',
        data: searchItem,
        headers: {
          "Authorization": "Basic " + btoa(username + ":" + password)
        },
        success: function(data) {
            document.getElementById('output').innerHTML = '';

            document.getElementById('actionResult').innerHTML = '';
            i = 0;
            printInfos(data['stats'], document.getElementById('output'));
            $.each(data['defs'], iterProviders);
            iterProviders('fallback', data['fallback']);
            iterProviders('notes', data['notes']);
            lastItemSearched = searchItem;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('jqXHR:'); console.log(jqXHR); console.log('textStatus:'); console.log(textStatus); console.log('errorThrown:'); console.log(errorThrown);
            console.log('Caller stack: ' + callingState.stack);
            // access stack during "error" callback
            console.log('Current stack: ' + (new Error()).stack);
        }
    });
}

function docKeyDown(e) {
    if (!(username)) {
        runLoadCreds();
    }
    if (['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].includes(e.key)) {
        e.stopPropagation();
        e.preventDefault();
        setNote({ "data": { "defId": e.key } });
        return false;
    }
}
document.addEventListener('keydown', docKeyDown, false);
document.getElementById('hz').addEventListener('keyup', findNotes, false);
