# Archived

This has been merged into the main site, see [here](https://github.com/transcrobes/transcrobes)


# Notrobes
This is the web extension for interacting with the learer's lexical database (adding, tagging, updating) of Transcrobes (https://transcrob.es).

Documentation
=============
See https://transcrob.es

Status
======
This is PRE-alpha software with KNOWN DATALOSS BUGS. It works, sorta, if you hold it right. There are not yet any tests and you should not attempt to use this project for anything like "production" until at least tests have been added.

The code in this extension was obviously not written by a developer (me!) who knows anything about writing modern web UIs. It will have to be completely rewritten from scratch by a competent web developer. In the meantime, it can function as a technology preview for what a properly written extension should do functionally (minus the bugs).

Installation
============

At some point the extension will need to be validated by the Mozilla folks and put on the public extensions site but that is unlikely to happen before the aforementioned complete rewrite.

For security reasons, users will have to compile their own extensions until that time.

As such, you will need to create a build of the extension and load it manually into all the browser installations you intend to use the extension on. The easiest way to do this is to install "web-ext" (see the Mozilla/Firefox docs for this) and then run

$ web-ext build

in the src/ directory. This will create a web-ext-artifacts directory, with the latest version in a .zip file.

You will also need to install in a "dev" version of the Firefox desktop app (so the official Developer version, or a Nightly version) and enable the installation of non-signed extensions on both Firefox desktop and mobile. You do the latter by going to "about:config" and then changing the value of "xpinstall.signatures.required" from "true" to "false". You can then go to "about:addons" and choose "Install add-on from file..." or simply put the file on a server somewhere named firefox@notrobes.transcrob.es.xpi and access the file like a normal URL in firefox (either desktop or mobile). You will be asked for confirmation to install an unsigned extension, which you must accept.

Configuration
=============

There are currently no public Transcrobes servers so you will need your own server installation for this extension to be of any use. See https://transcrob.es on how to install a Transcrobes server.

You will also need a user to be created on the server, a username and password for accessing the server, along with the server's publicly accessible URL.

You should then go to the extension properties page and fill in these values. Until you fill in correct values there, clicking the "Notrobes" button/menu entry will open the UI but it won't do anything. Error messages only appear in the developer console.

## Developer Certificate of Origin
Please sign all your commits by using `git -s`. In the context of this project this means that you are signing the document available at https://developercertificate.org/. This basically certifies that you have the right to make the contributions you are making, given this project's licence. You retain copyright over all your contributions - this sign-off does NOT give others any special rights over your copyrighted material in addition to the project's licence.

Development
===========

## Developer Certificate of Origin
Please sign all your commits by using `git -s`. In the context of this project this means that you are signing the document available at https://developercertificate.org/. This basically certifies that you have the right to make the contributions you are making, given this project's licence. You retain copyright over all your contributions - this sign-off does NOT give others any special rights over your copyrighted material in addition to the project's licence.

## Contributing
See [the website](https://transcrob.es/page/contribute) for more information. Please also take a look at our [code of conduct](https://transcrob.es/page/code_of_conduct) (or CODE\_OF\_CONDUCT.md in this repo).
